# Build le thème

Si vous avez le projet cloné en local vous devez `build` le thème pour récupérer l'archive.

A la racine du projet, exécutez cette commande:

```bash
yarn && yarn build && yarn build-keycloak-theme
```

# Récupérer le thème

Une fois la commande terminée, rendez vous dans `build_keycloak/target`. Vous trouverez l'archive du thème
