# Procédure Keycloakify

Le but de ce document est de présenter de façon simplifiée comment créer un thème Keycloak avec keycloakify.

## Setup l'environnement de test

1. Cloner en local le [projet](https://github.com/keycloakify/keycloakify-starter)
2. Dans le projet, il faut enlever les commentaires à la ligne 97 du fichier **src/keycloak-theme/login/kcContext.ts**
   ```tsx
   mockPageId: "login.ftl";
   ```
3. Dans un terminal,

```shell
yarn && yarn build && yarn start
```

4. Le projet run sur `localhost:3000` ou sur `localhost:3001` si le port 3000 est déjà occupé. Le nombre s'incrémente selon le nombre de serveur lancés.

## Modifier le thème

Toutes les modifications se font dans la partie **src/keycloak-theme/login**. Cela représente la page de connexion pour l'utilisateur. Il est possible aussi de modifier la partie gestion de compte via le dossier **account**.

### Dans le dossier **login**, il y a quelques fichiers et dossiers qui vont servir :

- **assets** : qui permet de mettre les images dont on va se servir. 

  Il est possible aussi d'ajouter un logo qui pourra être ajouté dans l'écran. **Il doit lui aussi respecter la notation logo.qqch**.
  :::warning
  Si vos images ne sont pas au format SVG aller à la section [Mes images ne sont pas en SVG](#Mes-images-ne-sont-pas-en-SVG).

- **Template.tsx** : Permet de modifier l'aspect général de la _card_ de connexion et la page de connexion en général.
Il est a quelques modification à faire dans cette page pour être en accord avec tous les thèmes Keycloak. Au début du fichier ce trouve le Header qui est identifiable avec la classe **header-style**.
Idem pour la div du footer qui est identifiable par la classe **footer-style**.
Ces deux classes sont définit dans le fichier **login.css** du dossier public.
Il est aussi important d'importer les logos avec la syntaxe suivante:
```javascript
import logo from "./assets/mon_logo.qqch";
```

- **pages/Login.tsx** : Permet de modifier l'interieur de la _card_ de connexion (champs de saisie principalement).

### Dans le dossier **public**

- **public/favicon.ico** : À remplacer avec le même nom si vous souhaitez remplacer l'icone affichée dans l'onglet du navigateur.
- **public/index.html** : Dans ce fichier vous pouvez définir le nom de l'onglet dans le navigateur. Pour ce faire, il faut modifier la ligne 16 :
  ```html
  <title>Apps Education - authentification</title>
  ```

#### Dans le sous-dossier **keycloak-ressources/ressources/**
- **/img** : Pour ajouter un arrière plan personnalisé il ajouter l'image de background ici. Il est plus simple de le nommer **background.qqch**
  :::warning
  Attention, le fichier doit garder le nom **background**.qqch c'est plus simple pour la suite.
  :::
- **/css/login.css** : Il y a pas mal de choses à faire dans ce fichier. C'est dans ce fichier que se trouve les classes **header-style** et **footer-style**. De plus si de nouvelle classes douvent être ajoutée, c'est dans ce fichier qu'elle doivent être définient.

### À la racine du projet

- **package.json** : Permet de définir le nom du thème grâce à l'attribut _name_. Ce nom apparaîtra notamment dans la sélection de thème au niveau de l'interface administrateur.

## Builder le thème et tester

Une fois votre thème terminé, il ne reste plus qu'à générer le fichier _jar_.
Pour ce faire rendez-vous dans le terminal :

```shell
yarn build-keycloak-theme
```

Une fois le script terminé, allez dans **build_keycloak/target**. Votre thème sera nommé :

    <nom de votre thème>-keycloak-theme-<numéroDeVersion>.jar

Il ne reste plus qu'à ajouter ce fichier dans votre instance keycloak dans le dossier **/opt/jboss/keycloak/standalone/deployments/**

## Autres informations

### Mes images ne sont pas en SVG

Si vos images ne sont pas en SVG ce n'est pas grave. Il faut aller modifier en conséquence les appels dans le code.

- Pour l'image de fond, aller dans le fichier **src/keycloak-theme/login/KcApp.css** et modifier la déclaration suivante :
  `css .my-root-class body { background: url(./assets/background.QQCH) no-repeat center center fixed; } `
  Il suffit de remplacer QQCH par l'extention de votre fichier.

### Ajouter un logo à la place du nom du Realm

Par défault dans le thème keycloak, le nom du realm est affiché au dessus de la _card_ de connexion. Si jamais cela ne vous convient pas, il est plutôt simple de le remplacer par un logo.

Pour ce faire ajouter votre logo dans le dossier **src/keycloak-theme/login/assets**

Une fois fait, aller dans le fichier **/login/Template.tsx**. Au début du fichier vous aller importer votre logo comme ceci:

```tsx
import logo from "./assets/logo.png";
```

Esuite aller à la ligne 56:

```html
{msg("loginTitleHtml", realm.displayNameHtml)}
```

Commentez cette ligne et mettez à la place :

```html
<img src="{logo}" alt="Mon super logo !" />
```

Il suffira ensuite d'ajouter les attribut CSS nécessaires via l'attribut _style_ de la balise _img_.
