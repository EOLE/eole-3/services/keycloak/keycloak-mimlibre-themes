# Comment mettre à jour le projet

1. Creer une référence vers le projet distant d'ORIGIN: 
   
    `git remote add upstream https://github.com/keycloakify/keycloakify-starter.git`

    Ansi une nouvelle référence **upstream** est créée.

2. Mettre à jour la nouvelle référence

    `git fetch upstream`

    La nouvelle référence est à jour

3. Fusionner la nouvelle référence avec notre projet:

    `git merge upstream/main`

    Après **résolution des conflits**, le projet est à jour avec le projet d'origine.